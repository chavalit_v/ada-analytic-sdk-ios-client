//
//  ADAAnalytics.h
//  ADAAnalytics
//
//  Created by Chavalit Vanasapdamrong on 25/5/2561 BE.
//

#import <Foundation/Foundation.h>

@protocol ADAAnalyticsDelegate

- (void)didSuccessLogEventToServer;
- (void)didFailLogEventToServer:(NSError *)error;

@end

@interface ADAAnalytics : NSObject

+ (instancetype)sharedInstance;

@property(nonatomic, assign) id<ADAAnalyticsDelegate> delegate;

- (void)setupWithAppId:(NSString *)mAppId baseUrl:(NSString *)baseUrl;
- (void)setExtraRequestHeader:(NSDictionary *)headers;
- (void)applicationDidEnterBackground;
- (void)applicationWillEnterForeground;
- (void)logEvent:(NSString *)eventName parameters:(NSDictionary *)parameters;

@end
